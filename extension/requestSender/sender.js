//for some xss findings
document.referrer = "<img src=x onerror=alert()>";
location.hash = "<img src=x onerror=alert()>";


function postData(url = '', data = '') {
    return fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: data, // тип данных в body должен соответвовать значению заголовка "Content-Type"
    })
    .then(function(response) {
           return response.text().then(function(text) {
             if (text == 'True') {
             	document.body.style.border = "5px solid red";
             }
        });
    });
}

postData('http://localhost:5000/', "url="+location.href)
  .then(data => console.log(data)) // JSON-строка полученная после вызова `response.json()`
  .catch(error => console.error(error));
