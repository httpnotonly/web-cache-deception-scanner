from pprint import pprint
import requests
from flask import Flask, request, jsonify, make_response

app = Flask(__name__)

payload = 'kek.kek'
xss_payload = 'kek.kek"onload="alert();'


def resp(data):
    resp = make_response(data)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@app.route('/', methods=['GET', 'POST'])
def check(xss=True):
    """
    Payload found in response
    :return:
    """
    url = request.form.get('url')
    xss = True if str(request.form.get('xss')).lower() == 'true' else False
    

    if not url:
        return resp('False')
        
    try:
        if not xss:
            return resp('True') if requests.get(url, headers={
                'X-Forwarded-Host': payload,
                'X-Forwarded-For': payload,
            }).text.find(payload) > -1 else resp('False')
        if xss:
            return resp('True') if requests.get(url, headers={
                'X-Forwarded-Host': xss_payload,
                'X-Forwarded-For': xss_payload,
            }).text.find(
                xss_payload) > -1 else resp('False')
    except Exception as ex:
        pprint(ex)
    return resp('False')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
