# Web Cache Deception scanner

At your linux run this
`docker run -d -p 5000:5000 --rm --name wcd_scanner registry.gitlab.com/httpnotonly/web-cache-deception-scanner:latest`

Go to `about:debugging` in your Firefox and select `manifest.json` from extention folder

Then just surf until you have red borders around web page
