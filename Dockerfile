FROM python:alpine3.7

COPY backend/ /
RUN pip install -r requirements.txt
EXPOSE 5000

CMD [ "python", "./wcdxff.py" ]
